# BioExcel Binder user guide

Apart from the authentication systems and some policies to restrict resource usage, the user experience is similar to the basic BInderHub service: the user can select a binder-compliant Git repository to launch, and then will be redirected to his own JupiterHub server.

## Quickstart

Try it running the [Ligand parameterization tutorial](https://github.com/bioexcel/biobb_wf_ligand_parameterization){target=_blank}: [![](https://mybinder.org/badge_logo.svg)](https://bioexcel-binder.tsi.ebi.ac.uk/v2/gh/bioexcel/biobb_wf_ligand_parameterization/master?filepath=biobb_wf_ligand_parameterization%2Fnotebooks%2Fbiobb_ligand_parameterization_tutorial.ipynb){target=_blank}.

## The BinderHub interface

![](assets/binderhub_form.png)

Provide in the above form a URL or a GitHub repository that contains Jupyter notebooks, as well as a branch, tag, or commit hash. **Launch** will build your Binder repository. If you specify a path to a notebook file, the notebook will be opened in your browser after building.

  
Binder will search for a **dependency file**, such as requirements.txt or environment.yml, in the repository's root directory ([more details on more complex dependencies in documentation](https://mybinder.readthedocs.io/en/latest/using.html#preparing-a-repository-for-binder){target=_blank}). The dependency files will be used to **build a Docker image**. 

If your Binder repository **has already been built once**, then subsequent clicks on the Binder link will not re-trigger the build process. However, if you **push any changes to the repository**, then it will be rebuilt the next time somebody clicks a link.

  
A [JupyterHub](https://jupyterhub.readthedocs.io/en/latest/){target=_blank} server will host your repository's contents.

## Policies

* Unless further specific customization, only **one instance is allowed per user**. This means that running a new instance will kill the previous one.
* Moreover (except for the training instance), **idle sessions are killed** after a pre-determined time out.
