# Welcome to the *BioExcel Binder* documentation

This documentation will help you to discover:

* **What is the "BioExcel Binder" project,** and what it is offering to the scientific community.
* How the **BioExcel Binder project is related to the BinderHub** open-source community.
* **How to access and use** the service.
* Details about **the technology powering the service** and **how it's deployed** over the cloud.

## Introduction

The BioExcel Binder portal allows users to get executable [Jupyter notebooks](https://jupyter-notebook.readthedocs.io/en/latest/notebook.html){target=_blank} for training, experimenting and reproducible data science. By using the portal the users can directly dive into the computation and analysis, skipping the challenges related to installing a working environment (OS packages, Python environments, Jupyter... ).

![](assets/jupyterhub_ux.png)

Basically, the portal is a [**BinderHub**](https://binderhub.readthedocs.io/en/latest/index.html){target=_blank} **dedicated instance** provided by BioExcel. The BinderHub is an *open-source* cloud service allowing users to share reproducible interactive computing environments from code repositories.

## Technical overview

Starting from a *software repository* (eg: the [BioBB workflow tutorials](https://mmb.irbbarcelona.org/biobb/workflows/){target=_blank}) the service creates a *personal virtual machine* (*pod*) running an instance of Jupyter and other specific tools.

![Binderhub](assets/binderhub_overview.png)

**"A Binder"** (also called a Binder-ready repository) is a code repository that contains at least two things:

* Code or content that you’d like people to run. This might be a Jupyter Notebook that explains an idea, or an R script that makes a visualization.
* Configuration files for your environment. These files are used by Binder to build the environment needed to run your code.

## MyBinder vs BioExcel Binder

"[**The Binder Project**](https://jupyter.org/binder){target=_blank}" is an open community, apart from building tools, it offers a public service called [MyBinder](https://mybinder.org/){target=_blank}. That service is *general-purpose* and not suitable for more complex cases such as computationally demanding pipelines (e.g. MD Setup) or training events (with many instances possibly running at the same time).

On the other hand, the **BioExcel Binder cloud infrastructure** is designed to offer specific capabilities to support scientific computations. It allows us to link demonstration tutorials to private, controlled infrastructure, overcoming possible overload problems of the public MyBinder

The BioExcel Binder instance includes customizations such as:

* authentication,
* greater computational resources per user,
* persistent storage for users,
* restrict sharing within a certain institution or team.

## The BioExcel Binder portal(s)

At the moment there are two separate portals for different use cases:

* **Closed training events.** Accessible upon authentication and authorization from the [BioExcel Cloud Portal](https://bioexcel.ebi.ac.uk/){target=_blank}.
    * Access for training participants only.
    * Optimisation of infrastructure for a defined number of users.
    * Shared file system to distribute documentation and files.

* [**Open instance**](https://bioexcel-binder.tsi.ebi.ac.uk){target=_blank} for independent experiments. It's accessible from the [BioBB workflows section](https://mmb.irbbarcelona.org/biobb/workflows){target=_blank} and the [GROMACS tutorials websites](https://tutorials.gromacs.org/){target=_blank}, with a simple GitHub authentication.
    * Facilitate “fair” sharing of infrastructure
    * Limit number of instances, kill idle sessions
    * Easy UX
    * No/lightweight authentication
